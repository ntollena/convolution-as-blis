CC=gcc
BUILD=./build
CFLAGS=-g -Wall -O3
LDFLAGS=
SRC=convolution.c
OBJ= $(SRC:.c=.o)
OBJ_PAPI= $(SRC:.c=.papi.o)
SSE_FLAG=-mssse3
AVX_FLAG=-march=core-avx2


AVX=y

assembly: $(BUILD)/conv.s

exec_papi: build
	$(BUILD)/conv.papi.exe
exec: build
	$(BUILD)/conv.exe

$(BUILD)/conv.s:build
	objdump -D $(BUILD)/conv.exe > $(BUILD)/conv.s

build:$(BUILD)/conv.exe $(BUILD)/conv.papi.exe

$(BUILD)/conv.papi.exe:$(patsubst %.papi.o, $(BUILD)/%.papi.o,$(OBJ_PAPI))
ifdef AVX
	$(CC) -DAVX $(CFLAGS) $(AVX_FLAG) -o $@ $^ $(LDFLAGS) -lpapi
else
	$(CC) -DSSE $(CFLAGS) $(SSE_FLAG) -o $@ $^ $(LDFLAGS) -lpapi
endif

$(BUILD)/conv.exe:$(patsubst %.o, $(BUILD)/%.o,$(OBJ))
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(BUILD)/%.papi.o:%.c
ifdef AVX
	$(CC) -DPAPI -DAVX $(CFLAGS) $(AVX_FLAG) -o  $@ -c $< $(LDFLAGS)
else
	$(CC) -DPAPI -DSSE -c $(CFLAGS) $(SSE_FLAG) -o $@ -c $< $(LDFLAGS)
endif

$(BUILD)/%.o:%.c
ifdef AVX
	$(CC) -DAVX $(CFLAGS) $(AVX_FLAG) -o $@ -c $< $(LDFLAGS)
else
	$(CC) -DSSE $(CFLAGS) $(SSE_FLAG) -o $@ -c $< $(LDFLAGS)
endif
