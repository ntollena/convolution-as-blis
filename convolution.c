#include <stdio.h>
#include <stdlib.h>
#include <x86intrin.h>
#include <papi.h>

/// statement that GCC (currently) cannot reorder around
#define COMPILER_BARRIER() asm volatile("" ::: "memory");

#define TI1 100
#define TJ1 100
#define TK1 100
#define TL1 100
#define TI2 1000
#define TJ2 1000
#define TK2 1000
#define TL2 1000

#define SIZE_I 128
#define SIZE_J 256
#define SIZE_K 384
#define SIZE_L 512
#define SIZE_M 3
#define SIZE_N 3

#define STRIDE_K (SIZE_K + SIZE_M - 1)
#define STRIDE_L (SIZE_L + SIZE_N - 1)
#define SIZE_OUTPUT (SIZE_J * SIZE_K * SIZE_L)
#define SIZE_INPUT (SIZE_I * STRIDE_K * STRIDE_L)
#define SIZE_PARAMS (SIZE_J * SIZE_I * SIZE_M * SIZE_N)

#ifdef AVX

#define VTYPE __m256
#define VECTOR_SIZE 8
#define V_ALIGN 32
#define VLOAD _mm256_load_ps
#define VSTORE _mm256_store_ps
#define VSET _mm256_set1_ps
#define VFMA(v1, v2, v3) _mm256_fmadd_ps((v1), (v2), (v3))

#elif defined SSE

#define VTYPE __m128
#define VECTOR_SIZE 4
#define V_ALIGN 16
#define VLOAD _mm_load_ps
#define VSTORE _mm_store_ps
#define VSET _mm_set1_ps
#define VFMA(v1, v2, v3) _mm_add_ps(_mm_mul_ps((v1), (v2)), (v3))

#else
#error "Should define either SSE or AVX"
#endif

#define TILE_K (6)
#define TILE_J (VECTOR_SIZE)

void convolution(float* __restrict__ output, float* const __restrict__ input, float* const __restrict__ params) {
	VTYPE inp, outp0[2], outp1[2], outp2[2], outp3[2], outp4[2], outp5[2], par1, par2;
	for (int j = 0; j < SIZE_J; j += TILE_J){
		for (int l = 0; l < SIZE_L; l++) {
			for (int n = 0; (n < SIZE_N); n++) {
				for (int k = 0; k < SIZE_K; k += TILE_K) {
					for (int m = 0; m < SIZE_M; m++) {
						int mk = k + m;
						int nl = l + n;
						outp0[0] = VLOAD(&output[j + k * SIZE_J + l * SIZE_J * SIZE_K]);
						outp0[1] = VLOAD(&output[VECTOR_SIZE + j + k * SIZE_J + l * SIZE_J * SIZE_K]);
						outp1[0] = VLOAD(&output[j + (k + 1) * SIZE_J + l * SIZE_J * SIZE_K]);
						outp1[1] = VLOAD(&output[VECTOR_SIZE + j + (k + 1) * SIZE_J + l * SIZE_J * SIZE_K]);
						outp2[0] = VLOAD(&output[j + (k + 2) * SIZE_J + l * SIZE_J * SIZE_K]);
						outp2[1] = VLOAD(&output[VECTOR_SIZE + j + (k + 2) * SIZE_J + l * SIZE_J * SIZE_K]);
						outp3[0] = VLOAD(&output[j + (k + 3) * SIZE_J + l * SIZE_J * SIZE_K]);
						outp3[1] = VLOAD(&output[VECTOR_SIZE + j + (k + 3) * SIZE_J + l * SIZE_J * SIZE_K]);
						outp4[0] = VLOAD(&output[j + (k + 4) * SIZE_J + l * SIZE_J * SIZE_K]);
						outp4[1] = VLOAD(&output[VECTOR_SIZE + j + (k + 4) * SIZE_J + l * SIZE_J * SIZE_K]);
						outp5[0] = VLOAD(&output[j + (k + 5) * SIZE_J + l * SIZE_J * SIZE_K]);
						outp5[1] = VLOAD(&output[VECTOR_SIZE + j + (k + 5) * SIZE_J + l * SIZE_J * SIZE_K]);
						for (int i = 0; i < SIZE_I; i ++){
							par1 = VLOAD(&params[j  + SIZE_J * i + SIZE_J * SIZE_I * m + SIZE_J * SIZE_I * SIZE_M * n ]);
							par2 = VLOAD(&params[VECTOR_SIZE + j  + SIZE_J * i + SIZE_J * SIZE_I * m + SIZE_J * SIZE_I * SIZE_M * n ]);
							// ITER 0
							inp = VSET(input[i + SIZE_I * mk + SIZE_I * STRIDE_K * nl]);
							outp0[0] =VFMA(inp,  par1,  outp0[0]);
							outp0[1] =VFMA(inp,  par2,  outp0[1]);
							// ITER 1
							inp = VSET(input[i + SIZE_I * (mk + 1) + SIZE_I * STRIDE_K * nl]);
							outp1[0] =VFMA(inp,  par1,  outp1[0]);
							outp1[1] =VFMA(inp,  par2,  outp1[1]);
							// ITER 2
							inp = VSET(input[i + SIZE_I * (mk + 2) + SIZE_I * STRIDE_K * nl]);
							outp2[0] =VFMA(inp,  par1,  outp2[0]);
							outp2[1] =VFMA(inp,  par2,  outp2[1]);
							// ITER 3
							inp = VSET(input[i + SIZE_I * (mk + 3) + SIZE_I * STRIDE_K * nl]);
							outp3[0] =VFMA(inp,  par1,  outp3[0]);
							outp3[1] =VFMA(inp,  par2,  outp3[1]);
							// ITER 4
							inp = VSET(input[i + SIZE_I * (mk + 4) + SIZE_I * STRIDE_K * nl]);
							outp4[0] =VFMA(inp,  par1,  outp4[0]);
							outp4[1] =VFMA(inp,  par2,  outp4[1]);
							// ITER 5
							inp = VSET(input[i + SIZE_I * (mk + 5) + SIZE_I * STRIDE_K * nl]);
							outp5[0] =VFMA(inp,  par1,  outp5[0]);
							outp5[1] =VFMA(inp,  par2,  outp5[1]);
						}
						VSTORE(&output[j + k * SIZE_J + l * SIZE_J * SIZE_K], outp0[0]);
						VSTORE(&output[VECTOR_SIZE + j + k * SIZE_J + l * SIZE_J * SIZE_K],outp0[1]);
						VSTORE(&output[j + (k + 1) * SIZE_J + l * SIZE_J * SIZE_K],outp1[0]);
						VSTORE(&output[VECTOR_SIZE + j + (k + 1) * SIZE_J + l * SIZE_J * SIZE_K],outp1[1]);
						VSTORE(&output[j + (k + 2) * SIZE_J + l * SIZE_J * SIZE_K],outp2[0]);
						VSTORE(&output[VECTOR_SIZE + j + (k + 2) * SIZE_J + l * SIZE_J * SIZE_K],outp2[1]);
						VSTORE(&output[j + (k + 3) * SIZE_J + l * SIZE_J * SIZE_K],outp3[0]);
						VSTORE(&output[VECTOR_SIZE + j + (k + 3) * SIZE_J + l * SIZE_J * SIZE_K],outp3[1]);
						VSTORE(&output[j + (k + 4) * SIZE_J + l * SIZE_J * SIZE_K],outp4[0]);
						VSTORE(&output[VECTOR_SIZE + j + (k + 4) * SIZE_J + l * SIZE_J * SIZE_K],outp4[1]);
						VSTORE(&output[j + (k + 5) * SIZE_J + l * SIZE_J * SIZE_K],outp5[0]);
						VSTORE(&output[VECTOR_SIZE + j + (k + 5) * SIZE_J + l * SIZE_J * SIZE_K],outp5[1]);
					}
				}
			}
		}
	}
}

void print_tab(FILE* file, float* tab, unsigned long size) {
  if (tab[0] * tab[0] < -5) {
    for (unsigned i = 0; i < size; i++) {
	  	fprintf(file, "%f\n", tab[i] );
	  }
  }
}

void init_zero(float* tab, unsigned long size) {
	for (unsigned i = 0; i < size; i++) {
		tab[i] = 0.;
	}
}

void init_par(float* tab, unsigned long size) {
	for (unsigned i = 0; i < size; i++) {
		tab[i] = ((float) i) / ((float) (i + 1)) ;
	}
}

void init_incr(float* tab, unsigned long size) {
	for (unsigned i = 0; i < size; i++) {
		tab[i] = ((float) i)  / 256. ;
	}
}

int main() {
#ifdef PAPI
  {
    const int retval = PAPI_library_init(PAPI_VER_CURRENT);
    if (retval != PAPI_VER_CURRENT) {
      const char *err = PAPI_strerror(retval);
      printf("%s:%d::PAPI_library_init failed: (%d) %s\n", __FILE__, __LINE__, retval, err);
      exit(1);
    }
  }
#endif

	float* input = (float *)aligned_alloc(V_ALIGN, SIZE_INPUT * sizeof(float));
	float* output = (float *)aligned_alloc(V_ALIGN, SIZE_OUTPUT * sizeof(float));
	float* params = (float *)aligned_alloc(V_ALIGN, SIZE_PARAMS * sizeof(float));
  if (input == NULL || output == NULL || params == NULL) {
    printf("failed to allocate buffers\n");
    return -1;
  }
	FILE * dev_null = fopen("/dev/null", "w");
	init_zero(output, SIZE_OUTPUT);
	init_incr(input, SIZE_INPUT);
	init_par(params, SIZE_PARAMS);
	//print_tab(input, SIZE_INPUT);
	//print_tab(params, SIZE_PARAMS);

  printf("size input: %ld, size_output: %ld\n",SIZE_INPUT * sizeof(float),SIZE_OUTPUT * sizeof(float)); 

#ifdef PAPI
  const char *EVENT_NAMES[] = {
    "CPU_CLK_UNHALTED",
  };
  const size_t num_events = sizeof(EVENT_NAMES) / sizeof(EVENT_NAMES[0]);

  int papi_eventset = PAPI_NULL;

  COMPILER_BARRIER();
  {
    // create event set
    {
      const int retval = PAPI_create_eventset(&papi_eventset);
      if (retval != PAPI_OK) {
        const char *err = PAPI_strerror(retval);
        printf("%s:%d::PAPI_create_eventset failed: (%d) %s\n", __FILE__, __LINE__, retval, err);
        exit(1);
      }
    }
    // add events
    for (size_t i = 0; i < num_events; i++) {
      const char *event_name = EVENT_NAMES[i];

      int event_code = 0;
      {
        const int retval = PAPI_event_name_to_code(event_name, &event_code);
        if (retval != PAPI_OK) {
          const char *err = PAPI_strerror(retval);
          printf("%s:%d::PAPI_event_name_to_code(\"%s\") failed: (%d) %s\n", __FILE__, __LINE__, event_name, retval, err);
          exit(1);
        }
      }
      {
        const int retval = PAPI_add_event(papi_eventset, event_code);
        if (retval != PAPI_OK) {
          const char *err = PAPI_strerror(retval);
          printf("%s:%d::PAPI_add_event(%d) (\"%s\") failed: (%d) %s\n", __FILE__, __LINE__, event_code, event_name, retval, err);
          exit(1);
        }
      }
    }
  }

  {
    const int retval = PAPI_start(papi_eventset);
    if (retval != PAPI_OK) {
      const char *err = PAPI_strerror(retval);
      printf("%s:%d::PAPI_start failed: (%d) %s\n", __FILE__, __LINE__, retval, err);
      exit(1);
    }
  }
  COMPILER_BARRIER();
#endif
	convolution(output, input, params);
#ifdef PAPI
  COMPILER_BARRIER();

  long long int papi_values[64];
  {
    const int retval = PAPI_stop(papi_eventset, papi_values);
    if (retval != PAPI_OK) {
      const char *err = PAPI_strerror(retval);
      printf("%s:%d::PAPI_stop failed: (%d) %s\n", __FILE__, __LINE__, retval, err);
      exit(1);
    }
  }
  COMPILER_BARRIER();

  for (unsigned i = 0; i < num_events; i++) {
    printf("%-30s %15lli\n", EVENT_NAMES[i], papi_values[i]);
  }
#endif

  print_tab(dev_null, output, SIZE_OUTPUT);
	free(input);
	free(output);
	free(params);
}
