open Batteries

type conv = {f: int list; c: int list; x: int list; y: int list; m: int option;
             n : int option} [@@deriving show {with_path = false}]

(* we are going to represent a given permutation by a dim list. Each dim
 * "consumes" a dimension in conv. 
 * There should be as many dim in dim_list as element in conv (sum of f, c...)
 * *)
type dim = F | C | X | Y | M | N [@@deriving show {with_path = false}]

let fresh_conv = {f =[]; c =[]; x =[]; y=[]; m = None; n = None}

let make_conv f c x y m n = {f=[f]; c=[c]; x=[x]; y=[y]; m = Some m; n = Some n}

(* Extract some level of tiling *)
let extract conv =
  let rec extract ({f = nf; c = nc; x = nx; y = ny; m = nm; n = nn} as new_conv)
  ({f;c;x;y;m;n} as conv)  = function
    | [] -> {f = List.rev nf; c = List.rev nc; x = List.rev nx; y = List.rev ny;
    m = nm; n = nn}
  | F::tl ->  extract {new_conv with f = (List.hd f)::nf}
                {conv with f = List.tl f} tl
  | C::tl ->  extract {new_conv with c = (List.hd c)::nc}
                {conv with c = List.tl c} tl
  | X::tl ->  extract {new_conv with x = (List.hd x)::nx}
                {conv with x = List.tl x} tl
  | Y::tl ->  extract {new_conv with y = (List.hd y)::ny}
                {conv with y = List.tl y} tl
  | M::tail ->  extract {new_conv with m = m} {conv with m = None} tail
  | N::tail ->  extract {new_conv with n = n} {conv with n = None} tail in
  extract fresh_conv conv

let rec last = function
  | [] -> failwith "Empty"
  | [t] -> t
  | _::tail-> last tail

let peak_perf {f;c;x;y;m;n} =
  let m = Option.default 1 m in
  let n = Option.default 1 n in
  let sf = last f in
  let sc = last c in
  let sx = last x in
  let sy = last y in
  (m * n * sf * sc * sx * sy / 16)

let conv = {f =[16; 128]; c = [128]; x = [6; 96; 384]; y = [16; 64]; m = Some 3; n
                                                                                 = Some 3}

let () = Format.printf "conv: %s; peak: %d\n" (show_conv conv) (peak_perf conv)
